# Training von tiefen eventbasierten neuronalen Netzen mit TTFS-Kodierung
Dies ist das GitLab-Repository zu meiner Bachelorarbeit. Die Daten der Parameter-Optimierung befinden sich in "Daten_Optimierung". Das Framework, von dem mein Code Teil ist, ist unter https://github.com/hbp-unibi/SNABSuite veröffentlicht. Der Code befindet sich in "SNABSuite", wobei für meine Arbeit vor allem getSpikeTimes in source/SNABs/mnist/helper_functions.cpp und die Funktionen
* MnistITLLastLayer::backward_path_TTFS
* MnistITLLastLayer::compute_backprop_mat
* MnistITLLastLayer::compute_TTFS_error
* MnistITLLastLayer::compute_TTFS_gradient
* MnistITLLastLayer::compute_gradients
* MnistITLLastLayer::update_mat_TTFS

in source/SNABs/mnist/mnist.cpp relevant sind und neu implementiert wurden. 

# Abstract
In dieser Arbeit wird das Training für tiefe eventbasierte neuronale Netze implementiert.
Die Daten werden innerhalb des Netzes als Aktionspotentiale zu bestimmten Zeitpunkten
kodiert, wobei frühe Aktionspotentiale großen Werten entsprechen. Die Kodierung
verwendet in jedem Neuron nur das erste Aktionspotential, wodurch insgesamt eine
schnellere Inferenz ermöglicht wird.

Der im Rahmen dieser Arbeit erstellte Code ist Teil eines bestehenden Software-
Frameworks, das den Vergleich von verschiedenen neuromorphen Hardware-Systemen
und Softwaresimulatoren vereinfacht. Nach der Implementierung wird die Software
beispielhaft genutzt, um Bilder des MNIST-Datensatzes mit handgeschriebenen Ziffern
zu klassifizieren. Verschiedene Parameter werden optimiert, um ein möglichst gutes
Ergebnis zu erhalten.

